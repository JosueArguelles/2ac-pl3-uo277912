#include <stdio.h>

void f(double * d)
{
    double x;
    x = 4;
    d = &x;
}

int main(int argc, char* argv[])
{
    double d;
    double * pDouble;

    d = 3;
    pDouble = &d; 

    f(pDouble);
    printf("%f\n", d);
    return 0;
}
